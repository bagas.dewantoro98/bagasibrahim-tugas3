import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {ProgressView} from '@react-native-community/progress-view';
export default function ProgressViewPage() {
  return (
    <View style={{justifyContent: 'center', padding: 20}}>
      <ProgressView
        progressTintColor="orange"
        trackTintColor="blue"
        progress={0.536}
      />
    </View>
  );
}
const styles = StyleSheet.create({});
