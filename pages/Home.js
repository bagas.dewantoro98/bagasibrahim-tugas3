import {
  Image,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
  TouchableOpacityComponent,
} from 'react-native';
import React from 'react';

export default function Home({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 65,
        paddingVertical: 30,
      }}>
      <ScrollView>
        <Text style={{fontSize: 20, alignSelf: 'center'}}>Main Page</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('NetInfoPage')}>
          <Text style={{fontSize: 20}}>Net Info</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('SliderPage')}>
          <Text style={{fontSize: 20}}>Slider</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('ProgressViewPage')}>
          <Text style={{fontSize: 20}}>Progress View</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('ProgressBarPage')}>
          <Text style={{fontSize: 20}}>Progress Bar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('ClipboardPage')}>
          <Text style={{fontSize: 20}}>Clipboard</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('PlaceHolder')}>
          <Text style={{fontSize: 20}}>Placeholder</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('WebViewPage')}>
          <Text style={{fontSize: 20}}>WebView</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'cyan',
    borderRadius: 5,
    width: '100%',
    alignItems: 'center',
    marginTop: 20,
  },
});
