import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Slider from '@react-native-community/slider';
export default function SliderPage() {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text style={{fontSize: 20}}>Slider</Text>
      <Slider
        style={{width: 250, height: 40}}
        minimumValue={0}
        maximumValue={1}
        minimumTrackTintColor="green"
        maximumTrackTintColor="blue"
      />
    </View>
  );
}
const styles = StyleSheet.create({});
