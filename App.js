import React from 'react';
import type {Node} from 'react';
import {
  Image,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './pages/Home';
import ProgressViewPage from './pages/ProgressViewPage';
import ProgressBarPage from './pages/ProgressBar';
import SliderPage from './pages/SliderPage';
import PlaceHolder from './pages/PlaceHolder';
import WebViewPage from './pages/WebViewPage';
import NetInfoPage from './pages/NetInfoPage';
import ClipboardPage from './pages/ClipboardPage';

const StackNav = createNativeStackNavigator();

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <StackNav.Navigator>
        <StackNav.Screen name="HomeBase" component={Home} />
        <StackNav.Screen name="NetInfoPage" component={NetInfoPage} />
        <StackNav.Screen name="SliderPage" component={SliderPage} />
        <StackNav.Screen name="ProgressViewPage" component={ProgressViewPage} />
        <StackNav.Screen name="ProgressBarPage" component={ProgressBarPage} />
        <StackNav.Screen name="PlaceHolder" component={PlaceHolder} />
        <StackNav.Screen name="WebViewPage" component={WebViewPage} />
        <StackNav.Screen name="ClipboardPage" component={ClipboardPage} />
      </StackNav.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default App;
